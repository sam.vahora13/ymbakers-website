const express = require('express');
const router = express.Router();
const home = require('./home');
const error = require('./error');


// GET home page.
router.route('/')
	.get(home.get);

router.route('/aboutus')
.get(home.getAbout);

router.route('/menu')
.get(home.getMenu);

router.route('/blog')
.get(home.getBlog);

router.route('/contact')
.get(home.getContact);

router.route('/login')
.get(home.getLogin);

router.route('/registration')
.get(home.registration);

router.route('*')
	.get(error.get404);

module.exports = router;
