
let request = require('request');
const querystring = require('querystring');
// Load the full build.
var _ = require('lodash');
let userId = "";

module.exports = (app, config) => {
    let url = config.baseUrl
    // console.log("config ::",config.baseUrl);

    // middleware function to check for logged-in users
    var sessionChecker = (req, res, next) => {
        if (req.session.user && req.cookies.user_sid) {
            res.redirect('/');
        } else {
            next();
        }    
    };

    // // route for Home-Page
    // app.get('/', sessionChecker, (req, res) => {
    //     res.redirect('/login');
    // });

    const checkLoggedIn = function (req, res, next) {
        // console.log("checkLoggedIn req.session.loggedIn >>",req.session.loggedIn);
        // if (req.session.loggedIn) {
        //     next();
        // } else {
        //     res.render("login", { url: url + 'api/login' });
        // }
    };


    // Admin Template routes
    app.get('/', function (req, res) {
        let email = req.session.email;
        let cart = "";
        // request.get({url: url +'/public/banner',json: true},(err, httpResponse, body)=>{
        //     if(!err){
        //         let bannerData = body.banner;
        //         request.get({url: url +'/public/displayItem',json: true},(err, httpResponse, itemBody)=>{
        //             if(!err){
        //                 // console.log('body ',itemBody.data);
        //                 console.log('userId ::',userId);
        //                 if(userId){
        //                     let data = {
        //                         userId:userId
        //                     }
        //                     request.post({url: url +'/public/userCartCount',body:data,json: true},(err, httpResponse, body)=>{
        //                         if(!err){
        //                             req.session.cart = body.cart;
        //                             console.log("Cart In ::",body.cart);
        //                             res.render('home', { url: url ,sliderImg: bannerData,token : req.session.token ,menus:itemBody.data,userId});
        //                         }
        //                     })
        //                 } else{
        //                     res.render('home', { url: url ,sliderImg: bannerData,token : req.session.token ,menus:itemBody.data,userId});
        //                 }
        //             }
        //         })
        //     }
        // })
        res.render('home', { url: url});
    });

    app.get('/aboutus', function (req, res) {
        // if (req.session.user && req.cookies.user_sid) {
        //     res.render('about', { url:url, baseUrl: url });
        // } else {
        //     res.redirect('/login');
        // }   
        res.render('aboutus', { url:url, baseUrl: url });     
    });

    app.get('/menu', function (req, res) {
        res.render('menu', { url:url, baseUrl: url });
    });

    app.get('/team', function (req, res) {
        res.render('team', { url:url, baseUrl: url });
    });

    app.get('/blog-home', function (req, res) {
        res.render('blog-home', { url:url, baseUrl: url });
    });

    app.get('/blog-single', function (req, res) {
        res.render('blog-single', { url:url, baseUrl: url });
    });

    app.get('/elements', function (req, res) {
        res.render('elements', { url:url, baseUrl: url });
    });

    app.get('/blog', function (req, res) {
        res.render('blog', { url:url, baseUrl: url });
    });

    app.get('/contact', function (req, res) {
        res.render('contact', { url:url, baseUrl: url });
    });

    app.get('/registration', function (req, res) {
        res.render('registration', { baseUrl: url });
    });

    app.get('/userCartPage', function (req, res) {

        res.render('userCart', { baseUrl: url ,userId,url : url});
    });

    // Login
    app.get('/login', function (req, res) {
        let options = {
            url: url + 'api/login',
        }
        if (req.session.user && req.cookies.user_sid) {
            res.redirect('/');
        } else {
            res.render('login', { url: options.url ,message:""});
        } 
    });

    // Log Out
    app.get('/logout', function (req, res) {
        let options = {
            url: url + 'api/login',
        }
        console.log('/logout ::');
        res.locals.user = null;
        // res.session.user = null;
        res.locals.token = null;
        // res.session.token = null;
        if (req.session.user && req.cookies.user_sid) {
            res.clearCookie('user_sid');
            res.redirect('/');
        } else {
            res.redirect('/login');
        }
        // res.redirect('/login');
        // request.get({url: url +'/public/banner',json: true},(err, httpResponse, body)=>{
        //     if(!err){
        //         let bannerData = body.banner;
        //         request.get({url: url +'/public/displayItem',json: true},(err, httpResponse, itemBody)=>{
        //             if(!err){
        //                 console.log('body ',itemBody.data);
        //                 res.render('home', { url: url ,sliderImg: bannerData,token : req.session.token ,menus:itemBody.data});
        //             }
        //         })
        //     }
        // })
    });

    app.post('/user-reg', function (req, res) {
        // console.log("req >>",req);
        let data = {
            email: req.body.email,
            password :req.body.password
        }
        if(data.email && data.password){
                    // console.log("data >>",data);
        request.post({url: url +'/public/login',body:data ,json: true},(err, httpResponse, body)=>{
            // console.log('err >>',err);
            // console.log('httpResponse >>',httpResponse);
            // console.log('body >>',body);
            let options = {
                url: url + 'api/login',
            }
            if(body.success === true){
                // console.log("err ::",err," body ::",body);
                req.session.user = body.user;
                res.locals.user = req.session.user;
                req.session.token = body.token;
                res.locals.token = req.session.token;
                userId = body.user.id;

                // req.session.user = body.user;
                // req.session.token = body.token;
                // res.locals.user = req.session.user;
                // res.locals.token = req.session.token;

                res.redirect('/');

                // request.get({url: url +'/public/banner',json: true},(err, httpResponse, body)=>{
                //     if(!err){
                //         let bannerData = body.banner;
                //         request.get({url: url +'/public/displayItem',json: true},(err, httpResponse, itemBody)=>{
                //             if(!err){
                //                 // console.log('body ',itemBody.data);
                //                 res.render('home', { url: url ,sliderImg: bannerData,token : req.session.token ,menus:itemBody.data});
                //             }
                //         })
                //     }
                // })
            } else {
                req.session.user = null;
                req.session.token = null;
                res.locals.user = null;
                res.locals.token = null;
                res.render('login',{message:"user not availabe"});
            }
        })
        } else {
            req.session.user = null;
            req.session.token = null;
            res.locals.user = null;
            res.locals.token = null;
            res.render('login',{message:"user not availabe"});
        }
    });

    app.get('/add-item-form', function (req, res) {
        console.log("Auth ::",req.session.token);
        request.get({url: url +'/public/category',json: true},(err, httpResponse, body)=>{
            console.log("body >>",body.category);
            if(!err){
                let options = {
                    url: url + 'api/login',
                }
                res.render('addItemForm', { url: url ,autoCategory:body.category ,token : req.session.token });
            }
        })
        
    });

    app.get('/add-edit-category', function (req, res) {
        res.render('addEditCategory', { url: url });
    });

    app.get('/add-edit-banner', function (req, res) {
        res.render('addEditBanner', { url: url });
    });


    // Category
    app.get('/addEditItem', checkLoggedIn, function (req, res) {
        console.log("Auth ::",req.session.token);
        let baseUrl = url + '/private/item'
        request.get({url: baseUrl,json: true},{headers:{Authorization:'Bearer '+req.session.token}},(err, httpResponse, body)=>{
            console.log("body >>",body);
            if(!err){
                res.render('addItemForm', { url:url,baseUrl: baseUrl });
            }else{
                alert('Check Login details..')
            }
        })
       
    });


    // app.get('/set-session', function (req, res) {
    //     res.locals.user = req.session.user;
    // });

    // Category
    app.get('/categories', checkLoggedIn, function (req, res) {
        let baseUrl = url + 'api/companies/categories-report'
        res.render('categories', { url:url,baseUrl: baseUrl });
    });

    app.get('/categories/sub', checkLoggedIn, function (req, res) {
        let baseUrl = url + 'api/companies/sub-categories-report'
        res.render('subcategories', { url:url, baseUrl: baseUrl });
    });

    // Company
    app.get('/company/categories', checkLoggedIn, function (req, res) {
        let baseUrl = url + 'api/companies/companies-by-category'
        res.render('companiesbycategories', { url:url, baseUrl: baseUrl });
    });

    app.get('/companiesbycategoryview', checkLoggedIn, function (req, res) {
        let name = req.query.category;
        let type = req.query.type;
        // let categoryName = name.Replace("&","%26");
        // console.log("categoryName >>",categoryName);
        let baseUrl = url + 'api/companies/companies-by-category-view?category=' + encodeURIComponent(name);
        if (type == 'full') {
            res.render('companiesbycategoriesfullview', { url : url, baseUrl: baseUrl })
        } else {
            res.render('companiesbycategoriesview', { url:url, baseUrl: baseUrl })
        }
    })

    app.get('/search-results', checkLoggedIn, function (req, res) {

        var searchPara = querystring.stringify(req.query);
        console.log("/search-results req.query >>",searchPara);
        let type = req.query.type;
        let baseUrl = url + 'api/companies/search-filter?' + searchPara
        if (type == 'full') {
            res.render('companiesbycategoriesfullview', {url : url, baseUrl: baseUrl })
        } else {
            res.render('companiesbycategoriesview', { url:url, baseUrl: baseUrl })
        }
    })

    app.get('/companydetails', checkLoggedIn, function (req, res) {
        let id = req.query.id;
        let baseUrl = url + 'api/companies/company-details?id=' + id
        let baseUrlForAutoComplate =  url + 'api/companies/company-info-autocomplate'

        request.get(baseUrlForAutoComplate, function (error, resp, body) {
            // console.log(body)
            let response = JSON.parse(body)
            if (response.success == false) {
                response.data = null
            }
            var autoComplteData = response.data;
            // console.log("baseUrlForAutoComplate >>",response.data);
            // res.render('companyDetails', { baseUrl: url, details: response.data })
            request.get(baseUrl, function (error, resp, body) {
                console.log(body)
                let response = JSON.parse(body)
                if (response.success == false) {
                    response.data = null
                }
                // console.log("autoComplate >>",autoComplteData);
                res.render('companyDetails', { url : url , baseUrl: baseUrl, autoComplate: autoComplteData, details: response.data })
            })

        })
    })

    app.get('/addupdatecompanyinfo', checkLoggedIn, function (req, res) {
        let id = req.query.id;
        let baseUrl = url + 'api/companies/company-details?id=' + id
        let baseUrlForAutoComplate =  url + 'api/companies/company-info-autocomplate'

        request.get(baseUrlForAutoComplate, function (error, resp, body) {
            // console.log(body)
            let response = JSON.parse(body)
            if (response.success == false) {
                response.data = null
            }
            var autoComplteData = response.data;
            // console.log("baseUrlForAutoComplate >>",response.data);
            // res.render('companyDetails', { baseUrl: url, details: response.data })
            request.get(baseUrl, function (error, resp, body) {
                // console.log(body)
                let response = JSON.parse(body)
                if (response.success == false) {
                    // response.data = null
                }
                // console.log("autoComplate >>",autoComplteData);
                res.render('addUpdateCompanyInfo', { url :url , baseUrl: baseUrl, autoComplate: autoComplteData, details: response.data })
            })

        })
    })   
    
    app.get('/search-company', checkLoggedIn, function (req, res) {
        let id = req.query.id;
        let baseUrl = url + 'api/companies/company-details?id=' + id
        let baseUrlForAutoComplate =  url + 'api/companies/company-info-autocomplate'

        request.get(baseUrlForAutoComplate, function (error, resp, body) {
            // console.log(body)
            let response = JSON.parse(body)
            if (response.success == false) {
                response.data = null
            }
            var autoComplteData = response.data;
            res.render('searchCompany', { url:url, baseUrl: baseUrl, autoComplate: autoComplteData })
        })
    }) 

    app.get('/company/subcategories', checkLoggedIn, function (req, res) {
        let baseUrl = url + 'api/companies/companies-by-sub-category'
        res.render('companiesbysubcategories', { url:url, baseUrl: baseUrl });
    });

    app.get('/companiesbysubcategoryview', checkLoggedIn, function (req, res) {
        let category = req.query.category;
        let subcategory = req.query.subcategory;
        let type = req.query.type;
        let baseUrl = url + 'api/companies/companies-by-sub-category-view?sub_category=' + subcategory + '&category=' + category
        if (type == 'full') {
            res.render('companiesbycategoriesfullview', { url : url, baseUrl: baseUrl })
        } else {
            res.render('companiesbycategoriesview', { url:url, baseUrl: baseUrl })
        }
    })

    app.get('/company/geography', checkLoggedIn, function (req, res) {
        let baseUrl = url + 'api/companies/companies-by-geography'
        res.render('companiesbygeography', { url:url, baseUrl: baseUrl });
    });

    app.get('/company/employee', checkLoggedIn, function (req, res) {
        let baseUrl = url + 'api/companies/companies-number-of-employ'
        res.render('companiesbyemployee', { url:url, baseUrl: baseUrl });
    });

    app.get('/company/interest', checkLoggedIn, function (req, res) {
        let baseUrl = url + 'api/companies/companies-of-interest'
        res.render('companiesofinterest', { url:url, baseUrl: baseUrl });
    });

    // Investment

    app.get('/investment/summary', checkLoggedIn, function (req, res) {
        let baseUrl = url + 'api/companies/investments-summary'
        res.render('investmentsummary', { url:url, baseUrl: baseUrl });
    });

    app.get('/investment/bycompany', checkLoggedIn, function (req, res) {
        let baseUrl = url + 'api/companies/investments-by-company'
        res.render('investmentbycompany', { url:url, baseUrl: baseUrl });
    });

    app.get('/investment/bycategory', checkLoggedIn, function (req, res) {
        let baseUrl = url + 'api/companies/investments-by-category'
        res.render('investmentbycategory', { url:url, baseUrl: baseUrl });
    });

    app.get('/investment/bysubcategory', checkLoggedIn, function (req, res) {
        let baseUrl = url + 'api/companies/investments-by-sub-category'
        res.render('investmentbysubcategory', { url:url, baseUrl: baseUrl });
    });

    // Sidebar

    app.get('/companylist', checkLoggedIn, function (req, res) {
        let baseUrl = url + 'api/companies/company-list'
        res.render('companiesbycategoriesfullview', { url : url , baseUrl: baseUrl })
    })

    app.get('/delMultiCompanylist', checkLoggedIn, function (req, res) {
        let baseUrl = url + 'api/companies/company-list'
        res.render('companyMultipleDelete', { url : url , baseUrl: baseUrl })
    })

    app.get('/company/upload-data-from-excel', checkLoggedIn, function (req, res) {
        let baseUrl = url + 'api/companies/companies-by-geography'
        let userId = req.session.userid;
        res.render('importExcelFile', { url:url, userId: userId, baseUrl: baseUrl });
    });


}