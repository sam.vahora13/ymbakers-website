const homeRoutes = {};

homeRoutes.get = (req, res) => {
	res.locals.user = req.user
	res.render('home');
}

homeRoutes.getAbout = (req, res) => {
	res.render('about');
}
homeRoutes.getMenu = (req, res) => {
	res.render('menu');
}
homeRoutes.getAbout = (req, res) => {
	res.render('about');
}
homeRoutes.getContact = (req, res) => {
	res.render('contact');
}
homeRoutes.getBlog = (req, res) => {
	res.render('blog');
}

homeRoutes.getLogin = (req, res) => {
	// console.log("ready!",req.locals.user);
	res.render('login');
}

homeRoutes.registration = (req, res) => {
	res.render('registration');
}


module.exports = homeRoutes;
