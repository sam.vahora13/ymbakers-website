const express = require('express');
const app = express();
require('dotenv').config();
const path = require('path');
var session = require('express-session')
const routes = require('./server/routes');
const cookieParser = require('cookie-parser');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));

// app.use(session({
//     secret: "sosecret",
//     saveUninitialized: false,
//     resave: false,
//     cookie: {
//         expires: 600000
//     }
// }));

// app.use(function(req, res, next) {
//     res.locals.user = req.session.user; 
//     res.locals.token = req.session.token;
//     next();
// });

// initialize cookie-parser to allow us access the cookies stored in the browser. 
app.use(cookieParser());

// initialize express-session to allow us track the logged-in user across sessions.
app.use(session({
    key: 'user_sid',
    secret: 'somerandonstuffs',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }
}));

// This middleware will check if user's cookie is still saved in browser and user is not set, then automatically log the user out.
// This usually happens when you stop your express server after login, your cookie still remains saved in the browser.
app.use((req, res, next) => {
    res.locals.user = req.session.user;
    res.locals.token = req.session.token;
    res.locals.cart = req.session.cart;
    if (req.cookies.user_sid && !req.session.user) {
        res.clearCookie('user_sid');        
    }
    next();
});




const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(routes);
let {config} = require('./config')
require('./server/routes/routes')(app, config);
app.listen(8000);
console.log("App listening on PORT:8080");
